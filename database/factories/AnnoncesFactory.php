<?php

namespace Database\Factories;

use App\Models\Annonces;
use Illuminate\Database\Eloquent\Factories\Factory;

class AnnoncesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Annonces::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence(),
            'content' => $this->faker->paragraph(),
            'author' => $this->faker->word(),
            'created_at' => now(),
        ];
    }
}