<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->id();
            $table->string('path')->default('default.jpg');
            $table->foreignId('annonce_id')->constrained()->onDelete('cascade');
            $table->timestamps();           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(Blueprint $table)
    {
        Schema::disableForeignKeyConstraints();
        $table->dropForeign(['annonce_id']);
        $table->dropColumn('annonce_id');
        Schema::dropIfExists('images');
    }
}