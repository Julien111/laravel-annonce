<?php

namespace Database\Seeders;

use App\Models\Annonces;
use Illuminate\Database\Seeder;

class AnnoncesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $annonces = new Annonces();
        $annonces->title = "Lorem ipsum";
        $annonces->content = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio eius adipisci laborum praesentium hic quidem voluptate nesciunt consectetur, delectus pariatur officiis quae eum laboriosam impedit sed harum nulla deserunt! Unde.";
        $annonces->author = "Jake";
        $annonces->created_at = now();
        $annonces->category_id = 1;
        $annonces->save();

        $annonces = new Annonces();
        $annonces->title = "Lorem two";
        $annonces->content = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio eius adipisci laborum praesentium hic quidem voluptate nesciunt consectetur, delectus pariatur officiis quae eum laboriosam impedit sed harum nulla deserunt! Unde.";
        $annonces->author = "John";
        $annonces->created_at = now();
        $annonces->category_id = 1;
        $annonces->save();

        $annonces = new Annonces();
        $annonces->title = "Lorem three";
        $annonces->content = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio eius adipisci laborum praesentium hic quidem voluptate nesciunt consectetur, delectus pariatur officiis quae eum laboriosam impedit sed harum nulla deserunt! Unde.";
        $annonces->author = "Tristan";
        $annonces->created_at = now();
        $annonces->category_id = 1;
        $annonces->save();

        $annonces = new Annonces();
        $annonces->title = "Lorem four";
        $annonces->content = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio eius adipisci laborum praesentium hic quidem voluptate nesciunt consectetur, delectus pariatur officiis quae eum laboriosam impedit sed harum nulla deserunt! Unde.";
        $annonces->author = "John";
        $annonces->created_at = now();
        $annonces->category_id = 2;
        $annonces->save();

        $annonces = new Annonces();
        $annonces->title = "Lorem five";
        $annonces->content = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio eius adipisci laborum praesentium hic quidem voluptate nesciunt consectetur, delectus pariatur officiis quae eum laboriosam impedit sed harum nulla deserunt! Unde.";
        $annonces->author = "John";
        $annonces->created_at = now();
        $annonces->category_id = 2;
        $annonces->save();

        $annonces = new Annonces();
        $annonces->title = "Lorem six";
        $annonces->content = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio eius adipisci laborum praesentium hic quidem voluptate nesciunt consectetur, delectus pariatur officiis quae eum laboriosam impedit sed harum nulla deserunt! Unde.";
        $annonces->author = "Marcus";
        $annonces->created_at = now();
        $annonces->category_id = 1;
        $annonces->save();

        $annonces = new Annonces();
        $annonces->title = "Lorem seven";
        $annonces->content = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio eius adipisci laborum praesentium hic quidem voluptate nesciunt consectetur, delectus pariatur officiis quae eum laboriosam impedit sed harum nulla deserunt! Unde.";
        $annonces->author = "Drake";
        $annonces->created_at = now();
        $annonces->category_id = 2;
        $annonces->save();

        $annonces = new Annonces();
        $annonces->title = "Lorem eight";
        $annonces->content = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio eius adipisci laborum praesentium hic quidem voluptate nesciunt consectetur, delectus pariatur officiis quae eum laboriosam impedit sed harum nulla deserunt! Unde.";
        $annonces->author = "Marc";
        $annonces->created_at = now();
        $annonces->category_id = 2;
        $annonces->save();

        $annonces = new Annonces();
        $annonces->title = "Lorem nine";
        $annonces->content = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio eius adipisci laborum praesentium hic quidem voluptate nesciunt consectetur, delectus pariatur officiis quae eum laboriosam impedit sed harum nulla deserunt! Unde.";
        $annonces->author = "Marcus";
        $annonces->created_at = now();
        $annonces->category_id = 1;
        $annonces->save();

        $annonces = new Annonces();
        $annonces->title = "Lorem ten";
        $annonces->content = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio eius adipisci laborum praesentium hic quidem voluptate nesciunt consectetur, delectus pariatur officiis quae eum laboriosam impedit sed harum nulla deserunt! Unde.";
        $annonces->author = "Marcos";
        $annonces->created_at = now();
        $annonces->category_id = 2;
        $annonces->save();
    }
}