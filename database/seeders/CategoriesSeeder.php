<?php

namespace Database\Seeders;

use App\Models\Categories;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = new Categories();
        $categories->name = "Voiture";
        $categories->created_at = now();
        $categories->save();

        $categories = new Categories();
        $categories->name = "Jardin";
        $categories->created_at = now();
        $categories->save();        
    }
}