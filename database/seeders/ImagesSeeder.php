<?php

namespace Database\Seeders;

use App\Models\Images;
use Illuminate\Database\Seeder;

class ImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $images = new Images();
        $images->path = "images/default.jpg";
        $images->annonce_id = 1;
        $images->created_at = now();
        $images->save();

        $images = new Images();
        $images->path = "images/default.jpg";
        $images->annonce_id = 2;
        $images->created_at = now();
        $images->save();

        $images = new Images();
        $images->path = "images/default.jpg";
        $images->annonce_id = 3;
        $images->created_at = now();
        $images->save();

        $images = new Images();
        $images->path = "images/default.jpg";
        $images->annonce_id = 4;
        $images->created_at = now();
        $images->save();

        $images = new Images();
        $images->path = "images/default.jpg";
        $images->annonce_id = 5;
        $images->created_at = now();
        $images->save();

        $images = new Images();
        $images->path = "images/default.jpg";
        $images->annonce_id = 6;
        $images->created_at = now();
        $images->save();

        $images = new Images();
        $images->path = "images/default.jpg";
        $images->annonce_id = 7;
        $images->created_at = now();
        $images->save();

        $images = new Images();
        $images->path = "images/default.jpg";
        $images->annonce_id = 8;
        $images->created_at = now();
        $images->save();

        $images = new Images();
        $images->path = "images/default.jpg";
        $images->annonce_id = 9;
        $images->created_at = now();
        $images->save();

        $images = new Images();
        $images->path = "images/default.jpg";
        $images->annonce_id = 10;
        $images->created_at = now();
        $images->save();
    }
}