<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\AnnoncesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('/contact', [HomeController::class, 'contact'])->name('contact');

//Pages par catégorie  

Route::get('/category/{category}', [HomeController::class, 'category'])->name('category');

//Affichage des annonces 

Route::get('/annonces', [AnnoncesController::class, 'index'])->name('annonces');

Route::get('/annonces/{id}', [AnnoncesController::class, 'show'])->name('annonce.show');

//Les routes pour notre partie profil

Route::get('/profil', [ProfilController::class, 'index'])->name('profil');

Route::get('/profil/create', [ProfilController::class, 'create'])->name('profil.create');
Route::post('/profil/create', [ProfilController::class, 'store'])->name('profil.store');

//modifier une annonce

Route::get('/profil/update', [ProfilController::class, 'update'])->name('profil.update');

Route::get('/profil/update/{id}', [ProfilController::class, 'edit'])->name('profil.edit');
Route::post('/profil/update/{id}', [ProfilController::class, 'change'])->name('profil.change');

//méthode delete 
Route::post('/profil/destroy/{id}', [ProfilController::class, 'destroy'])->name('profil.destroy');