@extends('layouts/base')

@section('content')
<div class="flex justify-center mt-8">
 <div class="shadow-md rounded-md overflow-hidden" style="width: 350px;">

       <div class="p-4">
          <h5 class="text-xl font-semibold mb-2">Contact</h5>

          <p class="mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iure sequi tenetur, voluptatibus
              harum
              consequuntur
              alias quaerat excepturi temporibus nisi commodi, ex, ratione quae soluta! Saepe alias dolores dolorem
              assumenda totam?</p>
        <div class="flex justify-center">
            <div>
            <a href="{{ route('home') }}"
                class="bg-purple-500 text-white active:bg-purple-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
            >
                Retour
            </a>
            </div>
        </div>
      </div>
  </div>
</div>
@endsection