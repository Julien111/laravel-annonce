@extends('layouts/profil')

@section('content')

<h1 class="text-gray-800 text-5xl text-red-800 text-center my-2">Partie profil user</h1>

<div class="flex justify-center mt-8">
 <div class="shadow-md rounded-md overflow-hidden" style="width: 350px;">

       <div class="p-4">
          
          <p class="mb-4">Sur la partie profil on peut créer une annonce, la modifier ou la supprimer. Il y a aussi la possibilité de rajouter une image pour chaque annonce.</p>
        <div class="flex justify-center">
            <div>
            <a href="{{ route('home') }}"
                class="bg-purple-500 text-white active:bg-purple-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
            >
                Retour à l'accueil
            </a>
            </div>
        </div>
      </div>
  </div>
</div>

@endsection