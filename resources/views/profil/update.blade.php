@extends('layouts/profil')

@section('content')

@if(session()->get('success'))
    <div class="bg-blue-100 border-t border-b border-blue-500 text-blue-700 px-4 py-3 my-3" role="alert">
      <p class="font-bold">{{ session()->get('success') }}</p>
    </div>
@endif

<h1 class="text-gray-800 text-5xl text-red-800 text-center my-2">Vos annonces</h1>

<div class="grid grid-cols-3 gap-2 mt-5">
@if($liste->count())
@foreach($liste as $elt)
<div class="max-w-md py-4 px-8 bg-gray-300 shadow-lg rounded-lg my-10">
  <div>
    <h2 class="text-gray-800 text-3xl font-semibold">{{ $elt->title }}</h2>    
  </div>
  <div class="flex justify-end mt-4">    
    <a href="{{ route('profil.edit', ['id' => $elt->id]) }}" class="bg-yellow-400 hover:bg-yellow-500 text-white font-bold mr-4 py-2 px-4 border border-blue-700 rounded">
      Modifier
    </a>
    <form action="{{ route('profil.destroy', $elt->id)}}" method="post">
    @csrf    
    <button class="bg-red-400 hover:bg-red-700 text-white font-bold py-2 px-4 border border-blue-700 rounded" type="submit">Supprimer</button>
  </form>
  </div>   
</div>  
   
@endforeach
</div>
@else  
    <div class="flex flex-col justify-center">
        <div class="bg-yellow-300 bg-opacity-25 ">
            <p class="text-black">Vous n'avez aucune annonce affichée.</p>
        </div>
    </div>
@endif

@endsection