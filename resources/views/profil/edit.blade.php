@extends('layouts/profil')

@section('content')

<h1 class="text-gray-800 text-5xl text-red-800 text-center my-2" >Modifier l'annonce n° {{ $annonce->id }} </h1>

@if ($errors->any())
<div class="flex flex-col items-center justify-center my-8">
    <div role="alert my-8">
		<div class="bg-red-500 text-white font-bold rounded-t px-4 py-2">
			Erreur
		</div>

        <ul  class="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">   
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif


<div class="flex flex-col items-center justify-center w-screen h-screen text-gray-700 mt-32">
	<form class="flex flex-col bg-white rounded shadow-lg p-12 mt-12" method="post" action="{{ route('profil.change', $annonce->id) }}" enctype="multipart/form-data">
		@csrf		
        <label class="font-semibold text-xs" for="title">Titre de l'annonce</label>
		<input name="title" class="flex items-center h-12 px-4 w-64 bg-gray-200 mt-2 rounded focus:outline-none focus:ring-2" value="{{ $annonce->title }}" id='title' type="text">
		<label class="font-semibold text-xs mt-3" for="content">Description de l'annonce</label>
		<textarea id="content" name="content" class="flex items-center h-52 px-4 w-64 bg-gray-200 mt-2 rounded focus:outline-none focus:ring-2" width="200px">{{$annonce->content}}</textarea>
		<label class="font-semibold text-xs" for="author">Auteur</label>
		<input name="author" class="flex items-center h-12 px-4 w-64 bg-gray-200 mt-2 rounded focus:outline-none focus:ring-2" value="{{ $annonce->author }}" id='author' type="text">
		<label class="font-semibold text-xs mt-2" for="category_id">Choisir la catégorie : </label>
		@foreach($categories as $category)
			@if ($category->id === $categorie_id)
				<div>
  					<input type="radio" id="{{$category->name}}" name="category_id" value="{{$category->id}}"
         			checked>
  					<label for="{{$category->name}}">{{$category->name}}</label>
				</div>
			@else
				<div>
  					<input type="radio" id="{{$category->name}}" name="category_id" value="{{$category->id}}">
  					<label for="{{$category->name}}">{{$category->name}}</label>
				</div>
			@endif
		@endforeach
		

		<div class="flex flex-col">
			<div>
				<label class="font-semibold text-xs mt-3" for="imgFile">Votre image :</label>
			</div>
			<div class="mt-3 justify-center md:justify-end">
        		<img class="w-20 h-20 object-cover rounded-full border-2 border-indigo-500" src="{{ Storage::url($annonce->image->path) }}">
  			</div>			  
		</div>

		<label class="font-semibold text-xs mt-3" for="imgFile">Modifier votre image :</label>

		<input class="mt-2" type="file"
		       id="imgFile" name="imgFile"
		       accept="image/png, image/jpeg, image/jpg">
	
        <button type="submit" class="flex items-center justify-center h-12 px-6 w-64 bg-blue-600 mt-8 rounded font-semibold text-sm text-blue-100 hover:bg-blue-700">Modifier</button>
		
	</form>

</div>

@endsection