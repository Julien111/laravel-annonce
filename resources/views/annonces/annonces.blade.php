@extends('layouts/base')

@section('content')

<h1 class="text-gray-800 text-5xl text-red-800 text-center my-2">Laravel annonces full</h1>

<div class="grid grid-cols-3 gap-2 mt-5">
@if($annonces->count())
@foreach($annonces as $annonce)
<div class="max-w-md py-4 px-8 bg-gray-300 shadow-lg rounded-lg my-10">
  <div class="flex justify-center md:justify-end -mt-16">
    <img class="w-20 h-20 object-cover rounded-full border-2 border-indigo-500" src="{{ Storage::url($annonce->image->path) }}">
  </div>
  
  <div>
    <h2 class="text-gray-800 text-3xl font-semibold">{{ $annonce->title }}</h2>
    <p class="mt-2 text-gray-600">{{ $annonce->content }}</p>
    <p class="text-xl my-2 text-blue-500">Author : {{ $annonce->author }}.</p>
  </div>
  <div class="flex justify-end mt-4">    
    <a href="{{ route('annonce.show', ['id' => $annonce->id]) }}" class="text-xl font-medium text-indigo-500">
      Voir Plus
    </a>
  </div>  
</div>  
   
@endforeach
</div>
@else  
    <div class="flex flex-col justify-center">
        <div class="bg-yellow-300 bg-opacity-25 ">
            <p class="text-black">Aucune annonce n'est pour l'instant diffusée.</p>
        </div>
    </div>
@endif



@endsection