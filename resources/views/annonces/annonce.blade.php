@extends('layouts/base')

@section('content')

<div class="flex justify-center justify-items-center mt-5">

<div class="max-w-md py-4 px-8 bg-gray-300 shadow-lg rounded-lg my-10">
  <div class="flex justify-center md:justify-end -mt-16">
        <img class="w-20 h-20 object-cover rounded-full border-2 border-indigo-500" src="{{ Storage::url($annonce->image->path) }}">
  </div>
  <div>
    <h2 class="text-gray-800 text-3xl font-semibold">{{ $annonce->title }}</h2>
    <p class="mt-2 text-gray-600">{{ $annonce->content }}</p>
    <p class="mt-2 text-gray-600">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Fugiat, aut similique optio expedita cupiditate voluptate tempora explicabo quibusdam neque doloremque, repudiandae, assumenda dignissimos quod accusamus ratione. Nihil quia eaque a?Accusantium tempore, dolore recusandae voluptate asperiores fuga aperiam, architecto adipisci repellendus similique autem. Veniam porro, unde reprehenderit modi placeat, eveniet nemo repellat eaque hic recusandae minima. Nemo distinctio consectetur adipisci!</p>
    <p class="mt-2 text-blue-600 font-medium">Catégorie : {{ $categorie->name }}.</p>
    <p class="mt-2 text-green-600 font-medium">Crée le : {{ $annonce->created_at->format('d m Y') }}.</p>
  </div>
  <div class="flex justify-end mt-4">
    <a href="#" class="text-xl font-medium text-indigo-500">Author : {{ $annonce->author }}</a>   
  </div>
  <div class="flex justify-items-center">
      <a href="{{ route('annonces') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">
        Retour
    </a>
  </div>
</div>

</div>

@endsection