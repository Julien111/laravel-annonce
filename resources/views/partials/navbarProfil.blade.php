<ul class="flex border-b">
  <li class="-mb-px mr-1">
    <a class="bg-white inline-block border-l border-t border-r rounded-t py-2 px-4 text-blue-700 font-semibold" href="{{ route('profil') }}">Profil</a>
  </li>
  <li class="mr-1">
    <a class="bg-white inline-block py-2 px-4 text-blue-500 hover:text-blue-800 font-semibold" href="{{ route('profil.create') }}">Créer une annonce</a>
  </li>
  <li class="mr-1">
    <a class="bg-white inline-block py-2 px-4 text-blue-500 hover:text-blue-800 font-semibold" href="{{ route('profil.update') }}">Modifier vos annonce</a>
  </li>  
  <li class="mr-1">
    <a class="bg-white inline-block py-2 px-4 text-yellow-500 hover:text-red-800 font-semibold" href="{{ route('home') }}">Retour à l'accueil</a>
  </li>
</ul>