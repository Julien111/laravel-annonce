<?php

namespace App\Http\Controllers;

use App\Models\Annonces;
use App\Models\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Affiche la page d'accueil du site
     *
     * @return void
     */
    public function index ()
    { 
        $categories = new Categories();
        $categorieElt = $categories->all();
        $annonces = new Annonces;
        $annonce = $annonces->orderByRaw('updated_at - created_at ASC')->limit(5)->get();
        $title = "Laravel Annonces";           
        return view('home/home', compact('title', 'annonce', 'categorieElt'));
    }

    /**
     * Affiche la page contact du site
     *
     * @return void
     */
    public function contact ()
    {
        $categories = new Categories();
        $categorieElt = $categories->all();

        return view('contact/contact', compact('categorieElt'));
    }

    /**
     * Affiche la page contact du site
     *
     * @return void
     */
    public function category (string $category)
    {

        $categories = new Categories();
        $categorieElt = $categories->all();
        
        $categoryElt = DB::table('categories')->select('id')->where('name', $category)->get();
        $categoryName = $category;
       
        $id = $categoryElt[0]->id;

        $eltAnnonces = new Annonces;
        $annonces = $eltAnnonces->where('category_id', $id)->get();

        //récupérer l'images de l'annonce         
        
        return view('category/categorie', compact('annonces', 'categorieElt', 'categoryName'));
    }

    
}