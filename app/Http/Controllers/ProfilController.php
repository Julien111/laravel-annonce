<?php

namespace App\Http\Controllers;

use App\Models\Images;
use App\Models\Annonces;
use App\Models\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProfilController extends Controller
{
    /**
     * Page d'accueil du profil
     *
     * @return void
     */
    public function index()
    {
        return view('profil/index');
    }

    /**
     * Page de creation d'annonce via formulaire
     *
     * @return void
     */
    public function create()
    {    
        $categories = Categories::all();
        return view('profil/create', compact('categories'));
    }

    /**
     * Methode store qui récupère les données du formulaire create
     *
     * @return void
     */
    public function store(Request $request)
    {   

        $validated = $request->validate([
                'title' => 'required|max:255|min:5|unique:annonces',
                'content' => 'required',
                'author' => 'required|min:3',
                'category_id' => 'required',
                'imgFile' => 'required',
        ]);
        
        
        $path = Storage::disk('public')->put('images', $request->imgFile);
                
        
        if($validated){
        
            $annonce = Annonces::create([
                'title' => $request->title,
                'content' => $request->content,
                'author' => $request->author,
                'category_id' => $request->category_id,
            ]);

            //On enregistre l' images en base de données

            $image = new Images;
            $image->path = $path;

            $annonce->image()->save($image);

        }    
        
        return redirect('/profil/update')->with('success', 'Annonce créée avec succès.');        
    }


    /**
     * Page qui liste les annonces pour ensuite les modifier 
     *
     * @return void
     */
    public function update()
    {    
        $annonces = new Annonces();

        $liste = $annonces->all();
        
        return view('profil/update', ['liste' => $liste]);
    }

    
    /**
     * Page qui va modifier une annonce selon l'id
     * @praram $id de l'annonce
     * @return void
     */
    public function edit($id)
    {    
        $annonce = Annonces::findOrFail($id);  
        $categorie_id= $annonce->category_id;
        $categories = Categories::all();     
                
        return view('profil/edit', compact('annonce', 'categories', 'categorie_id'));        
    }

    /**
     * Methode store qui récupère les données du formulaire update
     * Le but modifier une annonce
     *
     * @return void
     */
    public function change(Request $request, $id)
    {   
        //Si on a pas d'image on enregistre les datas sans l'image

        if(empty($request->imgFile)){
            $validated = $request->validate([
                'title' => 'required|max:255',
                'content' => 'required',
                'author' => 'required',
                'category_id' => 'required'
            ]);

            Annonces::whereId($id)->update($validated);                
            
        return redirect('/profil/update')->with('success', 'Annonce mise à jour avec succès.');     
        }

        
        if(!empty($request->imgFile)){
            
            $validated = $request->validate([
                'title' => 'required|max:255|min:5',
                'content' => 'required',
                'author' => 'required',
                'category_id' => 'required',                
            ]);
                

            if($validated && isset($request->imgFile)){            

                //retrouver l'image liée au fichier et la supprimer 
                $file = DB::table('images')->where('annonce_id', $id)->value('path');        
    
                //suppression du fichier images dans storage

                unlink(storage_path('app/public/'.$file));

                //On enregistre la nouvelle image

                $path = Storage::disk('public')->put('images', $request->imgFile);

                //On enregistre la nouvelle image en base de données

                DB::table('images')->where('annonce_id', $id)->update(['path' => $path]);      
                Annonces::whereId($id)->update($validated);
                        
                return redirect('/profil/update')->with('success', 'Annonce mise à jour avec succès.');
            }
        }
    }

    /**
     * Methode store qui récupère les données du formulaire update
     * Le but modifier une annonce
     *
     * @return void
     */
    public function destroy($id) 
    {
        //retrouver l'image liée au fichier et la supprimer 
        $file = DB::table('images')->where('annonce_id', $id)->value('path');        
    
        //suppression du fichier images dans storage

        unlink(storage_path('app/public/'.$file));
                
        $annonce = Annonces::findOrFail($id);
        $annonce->delete();

        return redirect('/profil/update')->with('success', 'Votre annonce a été supprimée.');
    }

}