<?php

namespace App\Http\Controllers;

use App\Models\Annonces;
use App\Models\Categories;
use Illuminate\Http\Request;

class AnnoncesController extends Controller
{
    /**
     * Page index des annonces
     *
     * @return void
     */
    public function index()
    {

        $categories = new Categories();
        $categorieElt = $categories->all();

        $annonces = Annonces::all();      
            
        return view('annonces/annonces', ['annonces' => $annonces, 'categorieElt' => $categorieElt]);
    }

    /**
     * Montre en détail une annonce
     *
     * @return void
     */
    public function show($id)
    {

        $categories = new Categories();
        $categorieElt = $categories->all();

        $annonce = Annonces::findOrFail($id);
        $categorie = Categories::find($annonce->category_id);       
    
        return view('annonces/annonce', ['annonce' => $annonce, 'categorie' => $categorie, 'categorieElt' => $categorieElt]);
    }
}