<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Categories extends Model
{
    use HasFactory;

    /**
     * fonction qui va lier nos category au annonces
     *
     * @return void
     */
    public function annonces ()
    {
    return $this->hasMany(Annonces::class);
    }
}