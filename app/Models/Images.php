<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    use HasFactory;

    protected $fillable = ['path', 'annonce_id'];

    /**
     * Lien entre images et annonces (récupérer une annonce avec l'image)
     *
     * @return void
     */
    public function annonce()
    {
        return $this->belongsTo(Annonces::class);
    }
}