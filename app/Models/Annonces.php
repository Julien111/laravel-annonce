<?php

namespace App\Models;

use App\Models\Categories;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Annonces extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'content', 'author', 'category_id'];

    public function categories()
    {
        return $this->belongTo(Categories::class);
    }

    /**
     * Get the image associated with the annonce
     *
     * @return void
     */
    public function image()
    {
        return $this->hasOne(Images::class, 'annonce_id');
    }
}